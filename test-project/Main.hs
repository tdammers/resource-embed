module Main where

import EmbeddedFiles
import Control.Monad
import Control.Applicative
import Foreign.C.String

main = do
    forM_ [0..numEmbeddedFiles-1] $ \i -> do
        (peekCAString $ embeddedFileName i) >>= putStr
        putStrLn ":"
        (peekCAStringLen $ embeddedFileContent i) >>= putStr
        putStrLn "-------"
