# resource-embed

Compiles data files into C code with accompanying header and a Haskell FFI
module for building self-contained binaries.

## Usage

First, run resource-embed to compile the relevant data files.

    resource-embed foo.js bar.css logo.png

This will produce three files: `embedded-files.c`, `embedded-files.h`, and
`EmbeddedFiles.hs`.

Then use a suitable C compiler to compile `embedded-files.c`, e.g.:

    gcc -c embedded-files.c -o embedded-files.o

Now, somewhere in your code, `import EmbeddedFiles`, and link
`embedded-files.o` into your binary.